class ChartData {
  final int x;
  final List<double> y;

  ChartData({required this.x, required this.y});
}

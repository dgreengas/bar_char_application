import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'startup_viewmodel.dart';

class StartUpView extends StatelessWidget {
  const StartUpView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<StartUpViewModel>.reactive(
      builder: (context, model, child) => Scaffold(
        appBar: AppBar(
          title: Text("Stacked Bar Chart"),
        ),
        body: AspectRatio(
          aspectRatio: 1.7,
          child: Card(
            elevation: 0,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
            color: const Color(0xff2c4260),
            child: BarChart(
              BarChartData(
                  alignment: BarChartAlignment.spaceAround,
                  maxY: 30,
                  barTouchData: BarTouchData(
                    enabled: false,
                    touchTooltipData: BarTouchTooltipData(
                      tooltipBgColor: Colors.transparent,
                      tooltipPadding: const EdgeInsets.all(0),
                      tooltipMargin: 8,
                      getTooltipItem: (
                        BarChartGroupData group,
                        int groupIndex,
                        BarChartRodData rod,
                        int rodIndex,
                      ) {
                        return BarTooltipItem(
                          rod.y.round().toString(),
                          TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        );
                      },
                    ),
                  ),
                  titlesData: FlTitlesData(
                    show: true,
                    bottomTitles: SideTitles(
                      showTitles: true,
                      getTextStyles: (value) => const TextStyle(
                          color: Color(0xff7589a2),
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                      margin: 20,
                      getTitles: (double value) {
                        switch (value.toInt()) {
                          case 0:
                            return 'Mn';
                          case 1:
                            return 'Te';
                          case 2:
                            return 'Wd';
                          case 3:
                            return 'Tu';
                          case 4:
                            return 'Fr';
                          case 5:
                            return 'St';
                          case 6:
                            return 'Sn';
                          default:
                            return '';
                        }
                      },
                    ),
                    leftTitles: SideTitles(showTitles: false),
                  ),
                  borderData: FlBorderData(
                    show: false,
                  ),
                  barGroups: model.chartData
                      .map<BarChartGroupData>((data) =>
                          BarChartGroupData(x: data.x, barRods: [
                            BarChartRodData(
                              y: data.y
                                  .reduce((value, element) => value + element),
                              /*colors: [
                                Colors.lightBlueAccent,
                                Colors.greenAccent
                              ],*/
                              rodStackItems: _convertToStackItem(data.y),
                            )
                          ], showingTooltipIndicators: [
                            0
                          ]))
                      .toList()),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.bar_chart,
          ),
          onPressed: () => model.changeData(),
        ),
      ),
      viewModelBuilder: () => StartUpViewModel(),
    );
  }

  List<BarChartRodStackItem> _convertToStackItem(List<double> data) {
    List<BarChartRodStackItem> results = [];
    double lastStart = 0;
    for (int i = 0; i < data.length; i++) {
      results.add(BarChartRodStackItem(
          lastStart, data[i] + lastStart, i == 0 ? Colors.red : Colors.blue));

      lastStart += data[i];
    }

    return results;
  }
}

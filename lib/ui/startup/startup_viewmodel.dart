import 'dart:math';

import 'package:bar_chart_application/data/chart_data.dart';

import 'package:stacked/stacked.dart';

class StartUpViewModel extends BaseViewModel {
  late List<ChartData> chartData;
  final Random _random = Random();

  StartUpViewModel() {
    _initializeChartData();
  }

  void _initializeChartData() {
    chartData = List<ChartData>.generate(7, (index) {
      int modIndex;
      switch (index) {
        case 0:
        case 1:
          modIndex = 0;
          break;
        case 2:
        case 3:
          modIndex = 1;
          break;
        case 4:
        case 5:
          modIndex = 2;
          break;
        case 6:
        case 7:
          modIndex = 3;
          break;
        case 8:
        case 9:
          modIndex = 4;
          break;
        case 10:
        case 11:
          modIndex = 5;
          break;
        default:
          modIndex = 6;
          break;
      }
      return ChartData(
          x: index, y: [_random.nextDouble() * 15, _random.nextDouble() * 15]);
    });
  }

  void changeData() {
    _initializeChartData();
    notifyListeners();
  }
}
